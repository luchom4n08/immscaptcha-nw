from PIL import Image, ImageOps
import numpy as np
import nwalign as nw 

minWhite = 230
class image:
	def __init__(self, name, adn):
		self.name = name
		self.adn = adn
def printBinaryNumpyImage(npImage):
	for i in range(0, len(npImage)):
		for j in range(0, len(npImage[0])):
			print("" if npImage[i][j] <= minWhite else "@"),
		print
	print

def rgbToADN(pixel):
	if pixel <= minWhite:
		return "A"
	else:
		return "G"
	
def imageToADN(path):
	img = Image.open(path).convert('L')
	
	np_img = np.array(img)
	printBinaryNumpyImage(np_img)
	vector_five = np_img.flatten()
	nw_five = [rgbToADN(x) for x in vector_five]

	return "".join(nw_five)

def NewImageFromPath(path, char):
	return image(char, imageToADN(path))


def matchImage(targetImage, testImages):
	result = {}
	max_score = 0
	scores = []
	print("target image: %s" % targetImage.name)
	for image in testImages:
		if image.name == targetImage.name:
			continue
		align1, align2 = nw.global_align(targetImage.adn, image.adn, gap_open=-10, gap_extend=-0.5, matrix='PAM250')
		score = nw.score_alignment(align1, align2, gap_open=-10,gap_extend=-0.5, matrix='PAM250')
		scores.append(score)
		if score > max_score:
			result = image
			max_score = score
		print("score: %d, image1: %s, image2: %s" % (score, targetImage.name, image.name))
	return result, max_score, scores

imgFive = NewImageFromPath("5.jpg", "5")
imgFiveTest = NewImageFromPath("5test.jpg", "5test")
imgThree = NewImageFromPath("3.jpg", "3")
imgEighTestRight = NewImageFromPath("8testR.jpg", "8testR")
imgEighTestTinyLeft = NewImageFromPath("8testTinyL.jpg", "8testTinyL")
imgEight = NewImageFromPath("8.jpg", "8")
imgV = NewImageFromPath("V.jpg", "V")
imgU = NewImageFromPath("U.jpg", "U")
imgVR = NewImageFromPath("VR.jpg", "VtestR")
imguL = NewImageFromPath("uL.jpg", "utestL")
imgVL = NewImageFromPath("VL.jpg", "VtestL")
imgvL = NewImageFromPath("vL.jpg", "vtestL")
imgv= NewImageFromPath("v.jpg", "v")
imgThreeTest = NewImageFromPath("3test.jpg", "3test")
imgSUpper = NewImageFromPath("S.jpg", "S")
testImages = [imgFiveTest, imgFive, imgV, imgVL, imguL, imgVR, imgv, imgvL, imgEight, imgEighTestRight, imgThree, imgEighTestTinyLeft, imgSUpper, imgThreeTest]
targetImage = imgV

result, max_score, _ = matchImage(targetImage, testImages)
print("test: %s, result: %s" % (targetImage.name, result.name))
